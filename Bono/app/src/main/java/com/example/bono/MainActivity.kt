package com.example.bono

import android.bluetooth.BluetoothAdapter
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.StrictMode
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import java.net.URL
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class MainActivity : AppCompatActivity() {

    //val descubrir : Button = findViewById(R.id.descubrir)
    val mBluetoothAdapter: BluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
    private lateinit var out : TextView
    private var date = LocalDateTime.now()
    private var cadenaformateador = DateTimeFormatter.ofPattern("d-M-YYYY")
    private var formateado= date.format(cadenaformateador)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val politica = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(politica)

        setContentView(R.layout.activity_main)

        out = findViewById(R.id.textView)
        if (mBluetoothAdapter == null) {
            out.append("device not supported")
        }

        descubrir.setOnClickListener{
            if (!mBluetoothAdapter.isDiscovering) {
                //out.append("MAKING YOUR DEVICE DISCOVERABLE");
                Toast.makeText (applicationContext, "MAKING YOUR DEVICE DISCOVERABLE",
                    Toast.LENGTH_LONG
                ).show()
                val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE)
                startActivityForResult(enableBtIntent, 0)

            }
        }

        confirmar.setOnClickListener{
            var mesAterior = formateado.split("-").get(1)
            var mesQuesEs = mesAterior.toInt()-1
            var stringHTTP = formateado.split("-")[0]+"-"+mesQuesEs+"-"+formateado.split("-")[2]



            try{
                URL("https://firestore.googleapis.com/v1/projects/attendancelistapp/databases/(default)/documents/attendance/"+stringHTTP+"/students/201613022")
                    .openStream().bufferedReader().use { it.readText() }

                Toast.makeText(this@MainActivity, "Asistió", Toast.LENGTH_SHORT).show()
            } catch (e: Exception)
            {
                Toast.makeText(this@MainActivity, "No está en la Base de datos", Toast.LENGTH_SHORT).show()
            }



        }


    }
}
